Imports System.Data
Imports System.Data.SqlClient
Imports System.Xml

Public Class Form1
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Lagre As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Lagre = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(6, 28)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(56, 20)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Artikkel ID"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(64, 24)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(176, 20)
        Me.TextBox1.TabIndex = 1
        Me.TextBox1.Text = ""
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(256, 24)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(80, 24)
        Me.Button1.TabIndex = 2
        Me.Button1.Text = "S�k"
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(64, 80)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(560, 20)
        Me.TextBox2.TabIndex = 3
        Me.TextBox2.Text = ""
        '
        'TextBox4
        '
        Me.TextBox4.Location = New System.Drawing.Point(64, 120)
        Me.TextBox4.Multiline = True
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.TextBox4.Size = New System.Drawing.Size(560, 408)
        Me.TextBox4.TabIndex = 5
        Me.TextBox4.Text = ""
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(6, 84)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(32, 20)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Tittel"
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(6, 120)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(48, 24)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Artikkel"
        '
        'Lagre
        '
        Me.Lagre.Location = New System.Drawing.Point(360, 24)
        Me.Lagre.Name = "Lagre"
        Me.Lagre.TabIndex = 6
        Me.Lagre.Text = "Lagre"
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(634, 549)
        Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.Lagre, Me.TextBox4, Me.TextBox2, Me.Button1, Me.TextBox1, Me.Label1, Me.Label2, Me.Label4})
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "Form1"
        Me.Text = "Editere Artikkel"
        Me.ResumeLayout(False)

    End Sub

#End Region

    'declare global variables
    Dim xmlDoc As XmlDocument = New XmlDocument()
    Dim title As XmlNode
    Dim ingress As XmlNode
    Dim body As XmlNode
    Dim refId As Integer

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'Prevent doing search when articleId is empty or when field doesn't contain an integer.
        Dim intTemp As Integer
        If (Me.TextBox1.Text <> "") Then
            Try
                intTemp = CInt(Me.TextBox1.Text)
            Catch
                MsgBox("Feltet 'Artikkel ID', m� inneholde en gyldig artikkel ID")
                Exit Sub
            End Try
            refId = Me.TextBox1.Text
            RunQuery()
        End If
    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged

    End Sub

    Private Sub TextBox4_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox4.TextChanged

    End Sub

    Public Sub RunQuery()
        Dim Message As String
        Dim myConnection As SqlConnection
        Dim mySqlCommand As SqlCommand
        Dim myDataReader As SqlDataReader
        Dim strXml As String
        Me.TextBox4.Text = ""

        myConnection = New SqlConnection("data source=DEVSERVER1;initial catalog=ntb;integrated security=SSPI;persist security info=False;user id=sa;workstation id=GX240-PORTAL-2;packet size=4096")
        mySqlCommand = New SqlCommand()
        mySqlCommand.Connection = myConnection

        mySqlCommand.CommandText = "getXml " & refId

        Try
            myConnection.Open()
            myDataReader = mySqlCommand.ExecuteReader(CommandBehavior.SingleRow)

        Catch err As Exception
            MsgBox(err.Message & vbCrLf & err.StackTrace)
            'Finally
        End Try

        Do While (myDataReader.Read())
            strXml = myDataReader.GetString(0)

        Loop
        myConnection.Close()

        'If strXml is nothing no data was retrived from database. Most likely because article id is invalid.
        If (strXml Is Nothing) Then
            MsgBox("Finner ikke �nsket artikkel. Sjekk om artikkel ID'en er gyldig.")
            Exit Sub
        End If
        xmlDoc.LoadXml(strXml)
        title = xmlDoc.SelectSingleNode("/nitf/body/body.head/hedline/hl1")
        body = xmlDoc.SelectSingleNode("/nitf/body/body.content")

        'writing content to textboxes
        Me.TextBox2.Text = title.InnerText
        Me.TextBox4.Text &= body.InnerXml.Replace("</p>", "</p>" & vbCrLf & vbCrLf).Replace("</hl2>", "</hl2>" & vbCrLf & vbCrLf)

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Lagre.Click

        Dim strIngress As String
        Dim strTitle As String
        Dim strXml As String
        Dim xmlDocTmp As XmlDocument = New XmlDocument()
        Dim mySqlCommand As SqlCommand
        Dim myConnection As SqlConnection

        Try
            'simple check to see if content in body is valid XML
            xmlDocTmp.LoadXml("<root>" & Me.TextBox4.Text & "</root>")

            'Updating XML title and body to reflect changes in inputfields
            title.InnerText = Me.TextBox2.Text
            body.InnerXml = Me.TextBox4.Text.Replace(vbCrLf & vbCrLf, vbCrLf)

        Catch err As XmlException
            MsgBox(err.Message)
        End Try

        'values to store to database is refId, strTitle, strIngress, and strXml file.
        title = xmlDoc.SelectSingleNode("/nitf/body/body.head/hedline/hl1")
        ingress = xmlDoc.SelectSingleNode("/nitf/body/body.content/p[@lede='true']")

        'check to see if article contains title and ingress.
        'If no title display msg and exit sub.
        'If no inngress give strIngress empty value, else give ingress value
        If (title.InnerText = "") Then
            MsgBox("Artikkelen m� ha en tittel. Denne skrives inn i tittelfeltet som tekst uten tagger.")
            Exit Sub
        End If
        If (ingress Is Nothing) Then
            strIngress = ""
        Else
            strIngress = ingress.InnerText
        End If

        strTitle = title.InnerText
        strXml = xmlDoc.InnerXml

        'all checks complete, starting procedure for updating article in database.
        myConnection = New SqlConnection("data source=DEVSERVER1;initial catalog=ntb;integrated security=SSPI;persist security info=False;user id=sa;workstation id=GX240-PORTAL-2;packet size=4096")
        mySqlCommand = New SqlCommand()

        myConnection.Open()

        mySqlCommand.CommandText = "[updateArticle]"
        mySqlCommand.CommandType = System.Data.CommandType.StoredProcedure
        mySqlCommand.Connection = myConnection
        mySqlCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RefID", System.Data.SqlDbType.Int, 4))
        mySqlCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ArticleTitle", System.Data.SqlDbType.VarChar, 255))
        mySqlCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Ingress", System.Data.SqlDbType.VarChar, 1024))
        mySqlCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ArticleXML", System.Data.SqlDbType.VarChar, 2147483647))

        With mySqlCommand
            .Parameters("@RefID").Value = refId
            .Parameters("@ArticleTitle").Value = strTitle
            .Parameters("@Ingress").Value = strIngress
            .Parameters("@ArticleXML").Value = strXml
            Try
                .ExecuteNonQuery()
            Catch err As Exception
                MsgBox(err.Message & vbCrLf & err.StackTrace)
            End Try

        End With

        myConnection.Close()

        xmlDoc.Save("TestOut.xml") 'remember to take this line out of the code

    End Sub

    Private Sub TextBox2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox2.TextChanged

    End Sub

    Private Sub TextBox3_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class
